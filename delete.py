import argparse

from subprocess import call

STOP_CONTAINER_COMMAND = ['docker', 'stop']
DELETE_CONTAINER_COMMAND = ['docker', 'rm', '-v']


def stop_container(name):
    cmd = STOP_CONTAINER_COMMAND
    cmd.append(name)
    call(cmd)


def delete_container(name):
    cmd = DELETE_CONTAINER_COMMAND
    cmd.append(name)
    call(cmd)

parser = argparse.ArgumentParser()
parser.add_argument('containers', nargs='+',
                    help='name of the containers you want to delete')


args = parser.parse_args()

for container in args.containers:
    stop_container(container)
    delete_container(container)
