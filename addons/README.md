This directory is used to store your addons.

Addons in this directory aren't tracked by git so this should be your working directory to write addons.