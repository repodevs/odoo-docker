import argparse
import os
import shutil
import subprocess
import tempfile

from config import DOCKER_IMAGE, INTERACTIVE_MODE, \
                   ODOO_VERSION, ODOO_PORT, \
                   ADDONS_PATH, CONFIG_PATH

POSTGRES_DOCKER_COMMAND = [
    'docker', 'run', '-d',
    '-e', 'POSTGRES_USER=odoo',
    '-e', 'POSTGRES_PASSWORD=odoo',
    # '-p', '5432:5432',  # only needed on macOS to reach the container
]


def create_database_container(name):
    cmd = list(POSTGRES_DOCKER_COMMAND)
    cmd.extend(['--name', '{}_db'.format(name), '-t', 'postgres:9.4'])
    subprocess.call(cmd)


def create_odoo_container(name, image, addons_path, port, version, dev):
    create_database_container(name)
    cmd = ['docker', 'run']

    dirs = [d for d in os.listdir(addons_path) if os.path.isdir(addons_path + '/' + d)]
    dir_paths = []

    for d in dirs:
        cmd.extend([
            '-v',
            '{}:/mnt/extra-addons/{}'.format(os.path.realpath(addons_path + '/' + d), d)
        ])
        dir_paths.append(',/mnt/extra-addons/{}'.format(d))

    change_addons_path(dir_paths, version)

    cmd.extend([
        '-v', '{}:/etc/odoo'.format(
            os.path.realpath(CONFIG_PATH)
        )
    ])
    cmd.extend([
        '-p', '{}:8069'.format(port),
        '--link', '{}_db:db'.format(name),
        '--name', name
    ])
    if dev:
        cmd.extend(['-i'])
    cmd.extend(['-t', '{}:{}'.format(image, version)])

    subprocess.call(cmd)


def change_addons_path(paths, version):
    base_addons = '/usr/lib/python2.7/dist-packages/openerp/addons,/mnt/extra-addons'
    file_path = CONFIG_PATH + '/openerp-server.conf'
    if version == 10.0:
        file_path = CONFIG_PATH + '/odoo.conf'
        base_addons = '/usr/lib/python2.7/dist-packages/odoo/addons,/mnt/extra-addons'


    file_desc, name = tempfile.mkstemp()

    path = 'addons_path = '

    if ',/mnt/extra-addons/enterprise' in paths:
        paths.remove(',/mnt/extra-addons/enterprise')
        path += '/mnt/extra-addons/enterprise,{}'.format(
            base_addons + ''.join(paths) + '\n'
        )
    else:
        path += base_addons + ''.join(paths) + '\n'

    with open(name, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if 'addons' in line:
                    line = path
                new_file.write(line)
    os.close(file_desc)
    os.remove(file_path)
    shutil.move(name, file_path)
    subprocess.call(['sudo', 'chmod', '-R', '777', CONFIG_PATH])

    # Mandatory otherwise odoo user will not be able to read the file
    subprocess.call(['sudo', 'chown', '-R', '104:1000', CONFIG_PATH])

parser = argparse.ArgumentParser()
parser.add_argument('odoo', help='name of the odoo container')
parser.add_argument(
    '-i', '--image', default=DOCKER_IMAGE,
    help='docker image to use (default to {})'.format(DOCKER_IMAGE)
)
parser.add_argument(
    '-a', '--addons', default=ADDONS_PATH,
    help='path of your addons folder (default to {})'.format(ADDONS_PATH)
)
parser.add_argument(
    '-p', '--port', type=int, default=ODOO_PORT,
    help='listening port of odoo (default to {})'.format(ODOO_PORT)
)
parser.add_argument(
    '-v', '--version', type=float, default=ODOO_VERSION,
    help='version of odoo you want to use (default to {})'.format(ODOO_VERSION)
)
parser.add_argument(
    '--dev', dest='dev', action='store_true',
    help='if specified, run the container in interactive mode')
parser.set_defaults(dev=INTERACTIVE_MODE)

args = parser.parse_args()
create_odoo_container(
    args.odoo, args.image,
    args.addons, args.port,
    args.version, args.dev
)
